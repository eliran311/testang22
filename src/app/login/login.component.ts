import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  public errorMessage:string; 
    constructor(private Authservice:AuthService, private router: Router,private route:ActivatedRoute) { 
    this.Authservice.getLoginErrors().subscribe(error => {
      this.errorMessage = error;
    });
  }
    ngOnInit() {
  
    }
  onSubmit(){
    this.Authservice.signin(this.email,this.password);
    
  
  }
  }
  