import { User } from './../interfaces/user';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  userId:string;
  email:string;
  password:string;
  constructor(public Authservice:AuthService,private router: Router,private route:ActivatedRoute) { }

  ngOnInit() {
    console.log("NgOnInit started") ;
}
onSubmit(){
   
  this.Authservice.signup(this.email,this.password);
    
  //  this.router.navigate(['/login']);
}
}