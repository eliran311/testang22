// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyBghk7yivao96P17t0HGRzUkwGqHSHCJL4",
    authDomain: "testangular-21f9e.firebaseapp.com",
    databaseURL: "https://testangular-21f9e.firebaseio.com",
    projectId: "testangular-21f9e",
    storageBucket: "testangular-21f9e.appspot.com",
    messagingSenderId: "532480853034",
    appId: "1:532480853034:web:628536d3eb60c0466a2f53",
    measurementId: "G-H403CV6PYZ"
  
}
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
